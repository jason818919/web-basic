module.exports = function(request, response, next){
	var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress || request.socket.remoteAddress || request.connection.socket.remoteAddress;
	var port = request.connection.remotePort;
	var start = Date.now();
	var url = request.url;
	var method = request.method;
	
	response.on("finish", function(){
		var duration = Date.now() - start;
		console.log('[SPEED] ' + ip+':'+port+' '+method + ' to ' + url + ' took ' + duration + ' ms');
	});
	next();
};
