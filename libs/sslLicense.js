const fs = require('fs');

//ssl license
const serverKeyPath = './ssl/server-key.pem';
const serverCertPath = './ssl/server-cert.pem';
const clientCertPath = './ssl/client-cert.pem';

const serverKey = fs.readFileSync(serverKeyPath);
const serverCert = fs.readFileSync(serverCertPath);
const clientCert = fs.readFileSync(clientCertPath);

// for https
const httpsOptions = {
	key: serverKey,
	cert: serverCert
};
// for tls
const tlsOptions = {
	key: serverKey,
	cert: serverCert,
	// This is necessary only if using the client certificate authentication.
	requestCert: true,
	// This is necessary only if the client uses the self-signed certificate.
	ca: [clientCert]
};

//ssl object
var ssl = {};

ssl.httpsOptions = httpsOptions; // for https
ssl.tlsOptions = tlsOptions; // for tls

module.exports = ssl;
