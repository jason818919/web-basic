$(document).ready(function(){
	console.log("web_basic.js is executing...");
	var socket = io.connect("/");
	socket.on("connect", function(){
		console.log("socket.io connection established...");
	});
	socket.on("disconnect", function(){
		console.log("socket.io disconnection");
	});
	socket.on("news", function(obj){
		console.log(obj);
	});
	socket.emit("my other event", "client hello!");
	socket.on("post_array", function(obj){
		console.log(obj);
		$("#post").empty();
		for(var i=0;i<obj.length;++i){
			$("#post").append("<p>"+obj[i]+"</p>");
		}
	});
	socket.emit("get_post", "hello");
	$('#message-submit').on('click', function(e){
		e.preventDefault();
		socket.emit("store_post", $('#message').val());
		$('#message').val("");
	});
	socket.on("source_post", function(obj){
		socket.emit("get_post", "hello");
	});
});
