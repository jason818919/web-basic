const HTTP_PORT = 41100; // make sure the ports are allowed to be connected
const HTTPS_PORT = 41101;
const TLS_SOCKET_PORT = 41102; // for TLS socket usage
const DB_PASS = 'accb97032b3578376308c2cdc60798d30a34756a';
require('console-stamp')(console, {
	metadata: function () {
		return ("[" + process.memoryUsage().rss + "]");
	},
	colors: {
		stamp: "yellow",
		label: "gray",
		metadata: "red"
	}
} );
const logger = require('./libs/logger'); // log all the request from the clients
const ssl = require('./libs/sslLicense'); // source the ssl keys
const url = require('url');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const https = require('https').Server(ssl.httpsOptions, app);
const redis = require('redis'), db = redis.createClient('/tmp/redis.sock');
const io = require('socket.io')(https);
const tls = require('tls'); //for TLS socket

db.auth(DB_PASS, (error, response) => {
	if(error){
		console.error('[DATABASE AUTH] error message: ' + error);
	}else{
		console.log('[DATABASE AUTH] pass message: ' + response);
	}
});

db.on('error', (error) => {
	console.error('[DATABASE ERROR] error message: ' + error);
});

db.on('ready', () => {
	console.log('[DATABASE READY] db client has established the connection to redis server');
	db.keys('*', (error, response) => {
		if(error){
			console.error('[DATABASE KEYS] error message: ' + error);
		}else{
			console.log('[DATABASE KEYS] number of key: ' + response.length);
			console.log('[DATABASE KEYS] all keys are as follows: ' + JSON.stringify(response));
		}
	});
});

app.use(logger);

app.use((request, response, next) => { // redirect the http to https
	if(!request.secure){
		var urlObj = url.parse('http://' + request.headers.host + request.url);
		var returnUrl = 'https://' + urlObj.hostname + ':' + HTTPS_PORT + urlObj.pathname;	
		console.log('[HTTP TO HTTPS] unsecure request, redirect to ' + returnUrl);
		return response.redirect(302, returnUrl);
	}
	next();
});

app.use(express.static(__dirname+'/public')); // allow everyone the gets the resource in public folder

app.get("/test", (request, response) => { // the page example
	response.sendFile(__dirname + '/views/test.html');
});

io.on('connection', (socket) => { // use socket.io as the communication bridge between client and server
	socket.emit('news', { hello: 'world' });
	socket.on('my other event', (data) => {
		console.log('[SOCKET.IO] my other event: ' + data);
	});

	socket.on('store_post', (obj) => {
		if(obj!=""){
			db.multi()
				.lpush('post:', obj)
				.ltrim('post:', 0, 19)
				.exec((error, response) => {
					if(error){
						console.error('[DATABASE LPUSH] error message: ' + error);
					}else{
						console.log('[DATABASE LPUSH] response message: ' + response);
						io.emit('source_post','hello');
					}
				});
		}
	});

	socket.on('get_post', (obj) => {
		db.multi()
			.lrange('post:', 0, -1)
			.exec((error, response) => {
				if(error){
					console.error('[DATABASE LRANGE] error message: ' + error);
				}else{
					console.log('[DATABASE LRANGE] response message: ' + response);
					socket.emit("post_array", response[0]);
				}
			});
	});
});

http.listen(HTTP_PORT, () => { // http listen on a specified port
	console.log('[HTTP] server start listening on port ' + HTTP_PORT + ' ...');
});

https.listen(HTTPS_PORT, () => { // https listen on a specified port
	console.log('[HTTPS] server start listening on port ' + HTTPS_PORT + ' ...');
});

// the following are for tls socket

var server = tls.createServer(ssl.tlsOptions, (client) => {
	if(client.authorized){
		console.log('[TLS SERVER] established a secure connection on ' + client.remoteAddress + ':' + client.remotePort);

		client.on('error', (error) => {
			console.error('[TLS SERVER] error message: ' + error);
			client.end();
		});

		client.on('end', () => {
			console.log('[TLS SERVER] disconnect on ' + client.remoteAddress + ':' + client.remotePort);
		});
		
		client.on('data', (data) => {
			console.log('[TLS SERVER] ' + client.remoteAddress + ':' + client.remotePort + ' say: ' + data);
			db.multi()
				.lpush('post:', data)
				.ltrim('post:', 0, 19)
				.exec((error, response) => {
					if(error){
						console.error('[DATABASE LPUSH] error message: ' + error);
					}else{
						console.log('[DATABASE LPUSH] response message: ' + response);
						io.emit('source_post','hello');
					}
				});

		});

		client.write('hello');
	}else{
		client.end();
	}
});

server.listen(TLS_SOCKET_PORT, () => {
	console.log('[TLS SERVER] TLS server start listening on port ' + TLS_SOCKET_PORT + ' ...');
});
