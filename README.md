# Synopsis
A web application framework based on [Node.js](https://nodejs.org/en/), [Express](http://expressjs.com), [socket.io](http://socket.io) and and [Redis](http://redis.io).
# Code Example
	
# Motivation
To simplify the ordinary routines on building web application.

# Installation
Download the source code and run the installation script as follows.
```bash

```

# API Reference

# Tests
Run the application on terminal.
```bash
node app.js
```
# Contributor
Jason Chen
